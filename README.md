# ExB Test - Frontend Photo Gallery

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
node
```

### Installing

A step by step that tell you how to get a development env running using npm

```
npm install
```

## Running the project

Execute the command 

```
npm start
```

It should take a few seconds for the project to be up and running on `http://localhost:4200`

## Building the project

To build the project, execute the command

```
npm build
```

It should be available in the `dist/` folder on your project source directory

## Running the tests

The project is using Jest for the unit tests, to check it out, run the command

```
npm run test
```

### Tests

The tests were made mostly for a proof of concept and not all of it is 100%.
Most of the work done was testing the api service requests as a mock.

## Built With

* [Angular Material](https://material.angular.io/) - Library for material design components
* [JSON Placeholder](https://jsonplaceholder.typicode.com) - API
* [Jest](https://jestjs.io/) - Library for Unit tests
* [RxJS](https://rxjs-dev.firebaseapp.com/) - Library for reactive programming using Observables

## Author

* **Jose Victor Guerra**

## Acknowledgments

Steps asked in the test that were successfully implemented

* Dinamic screen resizing for a minimum resolution of 1024x768 px
* Token Authentication proof of concept using interceptor
* Securing REST calls failures
* Ability to search for users
* Display of albums and pictures
* Creation of a new album (with REST call)
* Sorting for users and pictures
* Drag and Drop of albums to pictures section
* Pagination
* i18n

The order for choosing the nice-to-have features were

* Drag and Drop of albums
* Pagination
* Sorting
* A progress indicator for the loading of the pictures 
* Filters for user

This order was chosen for the purpose of enhancing the user experience as much as possible.
The last two items were not fully developed.

## Additional question:

Regarding a better user experience/interaction, what would you change/improve in the current
application?

* Change the backend API so the REST calls for the albums also brought the number of photos it has.
* A way to view the full-sized pictures
