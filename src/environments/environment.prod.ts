export const environment = {
  production: true,
  mock: false,
  endpoint: 'http://jsonplaceholder.typicode.com'
};
