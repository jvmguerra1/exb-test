import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import {
  MatSidenavModule,
  MatCardModule,
  MatCheckboxModule,
  MatButtonToggleModule,
  MatDividerModule,
  MatProgressBarModule,
  MatPaginatorModule,
  MatIconModule,
  MatButtonModule,
  MatInputModule,
  MatSelectModule,
  MatMenuModule
} from '@angular/material';

import { DragAndDropModule } from 'angular-draggable-droppable';
import { ExbCommonModule } from '../../common/exb-common.module';
import { UsersComponent } from './components/users/users.component';
import { PhotosComponent } from './components/photos/photos.component';
import { AlbumsComponent } from './components/albums/albums.component';
import { DashboardSandbox } from './dashboard.sandbox';
import { FormsModule } from '@angular/forms';

@NgModule({
  exports: [
    MatSidenavModule,
    MatCardModule,
    MatCheckboxModule,
    MatButtonToggleModule,
    MatDividerModule,
    MatProgressBarModule,
    MatPaginatorModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatMenuModule
  ]
})
export class MaterialModules {}

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    ExbCommonModule,
    DragAndDropModule,
    FormsModule,
    MaterialModules
  ],
  declarations: [
    DashboardComponent,
    UsersComponent,
    PhotosComponent,
    AlbumsComponent
  ],
  providers: [
    DashboardSandbox
  ]
})

export class DashboardModule { }
