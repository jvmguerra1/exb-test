import { Injectable } from '@angular/core';
import { ApiRequestsService } from '../../common/services/api-requests.service';
import { Observable, throwError } from 'rxjs';
import { User } from 'src/app/common/interfaces/User';
import { catchError } from 'rxjs/operators';
import { Album } from 'src/app/common/interfaces/Album';
import { Photo } from 'src/app/common/interfaces/Photo';
import { NewAlbum } from 'src/app/common/interfaces/NewAlbum';

@Injectable()
export class DashboardSandbox {

  constructor(private apiRequests: ApiRequestsService) { }

  public getUsers(): Observable<User[]> {
    return this.apiRequests.getUsers()
    .pipe(
      catchError(error => throwError(error || 'Server error'))
    );
  }

  public getAlbumsByUser(id: number): Observable<Album[]> {
    return this.apiRequests.getAlbumsByUser(id)
    .pipe(
      catchError(error => throwError(error || 'Server error'))
    );
  }

  public getPhotosByAlbum(id: number): Observable<Photo[]> {
    return this.apiRequests.getPhotosByAlbum(id)
    .pipe(
      catchError(error => throwError(error || 'Server error'))
    );
  }

  public createNewAlbum(title: string, userId: number): Observable<Album> {
    const album: NewAlbum = {title, userId};
    return this.apiRequests.createNewAlbum(album)
    .pipe(
      catchError(error => throwError(error || 'Server error'))
    );
  }

}
