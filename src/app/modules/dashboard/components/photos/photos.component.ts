import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Album } from 'src/app/common/interfaces/Album';
import { Photo } from 'src/app/common/interfaces/Photo';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit {

  @Input() set selectAll(albums: Album[]) {
    if (albums) {
      this.selectedAlbums = albums;
    }
  }
  @Input() selectedAlbums: Album[];
  @Input() set photos(value: Photo[]) {
    if (value) {
      this.allPhotos = value;
      this.visiblePhotos = this.calculatePagesPhotos(value);
    }
  }
  @Input() isLoading: boolean;
  @Input() showPlaceholder: boolean;
  @Input() showEmpty: boolean;
  @Input() showError: boolean;

  @Output() droppedAlbum: EventEmitter<number> = new EventEmitter<number>();
  @Output() clearAlbum: EventEmitter<number> = new EventEmitter<number>();
  public visiblePhotos: Photo[];
  private currentPage: number;
  private pageSize: number;
  public allPhotos: Photo[];

  constructor() { }

  ngOnInit() {
    this.initVariables();
  }

  public initVariables() {
    this.allPhotos = [];
    this.currentPage = 0;
    this.pageSize = 6;
  }

  private calculatePagesPhotos(photos: Photo[]): Photo[] {
    const previousImageIndex = this.currentPage * this.pageSize;
    return photos.slice(previousImageIndex, previousImageIndex + this.pageSize);
  }

  public emitClear(id: number) {
    this.clearAlbum.emit(id);
    this.initVariables();
  }

  public onDrop(event) {
    this.droppedAlbum.emit(event.dropData.album.id);
  }

  public sortAscending() {
    this.visiblePhotos.sort((a, b) => +(a.title > b.title) || -(a.title < b.title));
  }

  public sortDescending() {
    this.visiblePhotos.sort((a, b) => +(b.title > a.title) || -(b.title < a.title));
  }

  public pageChange(event: PageEvent) {
    if (event) {
      this.currentPage = event.pageIndex;
      this.pageSize = event.pageSize;
      this.visiblePhotos = this.calculatePagesPhotos(this.allPhotos);
    }
  }

}
