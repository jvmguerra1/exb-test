import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotosComponent } from './photos.component';
import { MatCardModule, MatMenuModule, MatIconModule, MatPaginatorModule, MatProgressBarModule } from '@angular/material';
import { AlbumChipComponent } from 'src/app/common/components/album-chip/album-chip.component';
import { PhotoCardComponent } from 'src/app/common/components/photo-card/photo-card.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';

export class FakeLoader implements TranslateLoader {
  public getTranslation(lang: string): Observable<any> {
      return of({});
  }
}

describe('PhotosComponent', () => {
  let component: PhotosComponent;
  let fixture: ComponentFixture<PhotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ MatCardModule, MatMenuModule, MatIconModule, MatPaginatorModule, MatProgressBarModule,
        TranslateModule.forRoot({
          loader: { provide: TranslateLoader, useClass: FakeLoader }
        })
      ],
      declarations: [ PhotosComponent, AlbumChipComponent, PhotoCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
