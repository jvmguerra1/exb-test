import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { User } from 'src/app/common/interfaces/User';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  @Input() set users(users: User[]) {
    this.filteredUsers = users;
    this.allUsers = users;
  }
  @Input() isLoading: boolean;
  @Input() showUserError: boolean;
  @Input() showUserNoData: boolean;
  @Output() userSelected: EventEmitter<boolean> = new EventEmitter<boolean> ();
  public searchValue: string;
  public searchInput: boolean;
  public allUsers: User[];
  public filteredUsers: User[];

  constructor() { }

  ngOnInit() {
    this.initVariables();
  }

  public initVariables() {
    this.searchInput = false;
    this.searchValue = '';
  }

  public toggleSearch(value: boolean) {
    this.searchInput = value;
    if (!value) {
      this.filteredUsers = this.allUsers;
    }
  }

  public userSelectChange(event) {
    this.userSelected.emit(event.value);
  }

  public sortAscending() {
    this.filteredUsers.sort((a, b) => +(a.name > b.name) || -(a.name < b.name));
  }

  public sortDescending() {
    this.filteredUsers.sort((a, b) => +(b.name > a.name) || -(b.name < a.name));
  }

  public filterUsers(input: string) {
    this.filteredUsers = this.allUsers.filter(user => user.name.toLowerCase().includes(input.toLowerCase()));
    if (!this.filteredUsers.length) {
      this.filteredUsers = this.allUsers;
    }
  }

}
