import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Album } from 'src/app/common/interfaces/Album';
import { MatDialog } from '@angular/material';
import { AddAlbumComponent } from 'src/app/common/components/add-album/add-album.component';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss']
})
export class AlbumsComponent {

  @Input() albums: Album[];
  @Input() isLoading: boolean;
  @Input() isEmpty: boolean;
  @Input() isError: boolean;
  @Output() selectAllEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() createAlbum: EventEmitter<string> = new EventEmitter<string>();
  numberOfPhotos = 1000;

  constructor(public dialog: MatDialog) { }

  public selectAll(event) {
    if (event.checked) {
      this.selectAllEmitter.emit(true);
    }
  }

  public sortAscending() {
    return this.albums ?
      this.albums.sort((a, b) =>
        +(a.title > b.title) || -(a.title < b.title))
      : new Array<Album>();
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(AddAlbumComponent, {
      width: '250px',
      data: ''
    });

    dialogRef.afterClosed().subscribe(result => {
      this.createAlbum.emit(result);
    });
  }

}
