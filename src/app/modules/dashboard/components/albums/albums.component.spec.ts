import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumsComponent } from './albums.component';
import {
  MatCardModule,
  MatCheckboxModule,
  MatProgressBarModule,
  MatButtonModule,
  MatIconModule,
  MatDialog,
  MatDialogRef
} from '@angular/material';
import { AlbumChipComponent } from 'src/app/common/components/album-chip/album-chip.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { of, Observable } from 'rxjs';
import { NO_ERRORS_SCHEMA } from '@angular/compiler/src/core';
import { Overlay } from '@angular/cdk/overlay';
import { InjectionToken } from '@angular/core';

export class FakeLoader implements TranslateLoader {
  public getTranslation(lang: string): Observable<any> {
      return of({});
  }
}

describe('AlbumsComponent', () => {
  let component: AlbumsComponent;
  let fixture: ComponentFixture<AlbumsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        MatCheckboxModule,
        MatProgressBarModule,
        MatButtonModule,
        MatIconModule,
        TranslateModule.forRoot({
          loader: { provide: TranslateLoader, useClass: FakeLoader }
        })
     ],
      declarations: [ AlbumsComponent, AlbumChipComponent ],
      providers: [
        {provide : MatDialogRef, useValue : {}},
        {provide : MatDialog, useValue : {}}
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
