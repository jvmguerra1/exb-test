import { Component, OnInit } from '@angular/core';
import { DashboardSandbox } from './dashboard.sandbox';
import { User } from 'src/app/common/interfaces/User';
import { Album } from 'src/app/common/interfaces/Album';
import { forkJoin } from 'rxjs';
import { Photo } from 'src/app/common/interfaces/Photo';
import { uniqBy } from 'lodash';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public users: User[];
  public selectedUser: number;
  public showUserError: boolean;
  public showUserLoading: boolean;
  public showUserNoData: boolean;

  public photos: Photo[];
  public showPhotoError: boolean;
  public showPhotoLoading: boolean;
  public showPhotoEmpty: boolean;

  public albums: Album[];
  public visibleAlbums: Album[];
  public selectedAlbums: Album[];
  public showAlbumsNoData: boolean;
  public showAlbumsError: boolean;
  public showAlbumsLoading: boolean;

  constructor(
    private dashboardSandbox: DashboardSandbox,
    public snackBar: MatSnackBar,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.initVariables();
  }

  private initVariables() {
    this.showUserError = false;
    this.selectedAlbums = [];
    this.photos = [];
    this.getUsersData();
  }

  private getUsersData(): void {
    this.showUserLoading = true;
    this.dashboardSandbox.getUsers().subscribe(
      res => this.getUsersDataSuccess(res),
      err => this.getUsersDataError()
    );
  }

  private getUsersDataSuccess(res: User[]): void {
    this.users = res;
    this.showUserNoData = !res.length;
    this.showUserError = false;
    this.showUserLoading = false;
  }

  private getUsersDataError(): void {
    this.showUserError = true;
    this.showUserLoading = false;
  }

  public getAlbumsByUser(id: number): void {
    this.showAlbumsLoading = true;
    this.selectedUser = id;
    this.dashboardSandbox.getAlbumsByUser(id).subscribe(
      res => this.getAlbumsByUserDataSuccess(res),
      err => this.getAlbumsByUserDataError()
    );
  }

  private getAlbumsByUserDataSuccess(res: Album[]): void {
    this.albums = res;
    this.visibleAlbums = res;
    this.showAlbumsNoData = !res.length;
    this.showAlbumsError = false;
    this.showAlbumsLoading = false;
  }

  private getAlbumsByUserDataError(): void {
    this.albums = [];
    this.visibleAlbums = [];
    this.showAlbumsError = true;
    this.showAlbumsLoading = false;
  }

  private getPhotosByAlbumSuccess(res: Photo[], albumId: number): void {
    this.showPhotoLoading = false;
    this.photos = [...this.photos, ...res];
    if (this.photos.length) {
      this.showPhotoError = false;
      this.albums.find(album => album.id === albumId).numberOfPhotos = res.length;
    } else {
      this.showPhotoEmpty = true;
    }
  }

  private getPhotosByAlbumError(): void {
    this.showPhotoError = true;
    this.showPhotoEmpty = false;
    this.showPhotoLoading = false;
    this.photos = [];
    this.selectedAlbums = [];
    this.visibleAlbums = this.albums;
  }

  public droppedAlbum(albumId: number) {
    this.selectedAlbums.push(...this.visibleAlbums.filter(album => album.id === albumId));
    this.visibleAlbums = this.visibleAlbums.filter(album => album.id !== albumId);
    this.getPhotosByAlbum(albumId);
    const photosRequests = this.selectedAlbums.map(album => this.dashboardSandbox.getPhotosByAlbum(album.id));
    forkJoin(photosRequests).subscribe(res => console.log([].concat.apply([], res)));
  }

  private getPhotosByAlbum(albumId: number): void {
    this.showPhotoLoading = true;
    this.dashboardSandbox.getPhotosByAlbum(albumId).subscribe(
      res => this.getPhotosByAlbumSuccess(res, albumId),
      err => this.getPhotosByAlbumError()
    );
  }

  public clearAlbum(albumId: number) {
    const filter = this.albums.filter(album => album.id === albumId);
    if (filter && filter.length === 1) {
      const addedAlbum = filter[0];
      this.visibleAlbums.push(addedAlbum);
      this.selectedAlbums = this.selectedAlbums.filter(album => album.id !== addedAlbum.id);
      this.photos = this.removeAlbumFromPhotos(albumId);
    }
  }

  public triggerSelectAllAlbums(value: boolean) {
    if (value) {
      const filteredArray  = this.filterRemainingAlbums();
      filteredArray.forEach(album => this.droppedAlbum(album.id));
      this.selectedAlbums = [...this.selectedAlbums, ...this.visibleAlbums];
      this.visibleAlbums = [];
    }
  }

  private filterRemainingAlbums(): Album[] {
    return this.visibleAlbums
      .filter(visitedAlbum => this.selectedAlbums
      .filter(selectedAlbum => selectedAlbum.id !== visitedAlbum.id));
  }

  private removeAlbumFromPhotos(albumId: number) {
    return uniqBy(this.photos.filter(photo => photo.albumId !== albumId), 'id');
  }

  public onCreateNewAlbum(event: string) {
    if (!this.selectedUser) {
      this.openSnackBar(this.translate.instant('DASHBOARD.SELECT_USER'));
    } else if (event) {
      this.showAlbumsLoading = true;
      this.dashboardSandbox.createNewAlbum(event, this.selectedUser).subscribe(
        res => this.onCreateNewAlbumSuccess(res),
        err => this.onCreateNewAlbumError()
      );
    } else {
      this.openSnackBar(this.translate.instant('DASHBOARD.ALBUM_TITLE_MISSING'));
    }
  }

  private onCreateNewAlbumSuccess(res: Album): void {
    this.openSnackBar(this.translate.instant('DASHBOARD.ALBUM_CREATED_SUCCESS') + res.id);
    this.getAlbumsByUser(this.selectedUser);
    this.showAlbumsLoading = false;
  }

  private onCreateNewAlbumError(): void {
    this.openSnackBar(this.translate.instant('DASHBOARD.ALBUM_CREATED_ERROR'));
    this.showAlbumsLoading = false;
  }

  public openSnackBar(message: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }

}
