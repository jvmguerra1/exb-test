import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../interfaces/User';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Album } from '../interfaces/Album';
import { Photo } from '../interfaces/Photo';
import { NewAlbum } from '../interfaces/NewAlbum';

@Injectable({
  providedIn: 'root'
})
export class ApiRequestsService {

  constructor(private http: HttpClient) { }

  public getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.endpoint}/users`);
  }

  public getAlbumsByUser(userId: number): Observable<Album[]> {
    return this.http.get<Album[]>(`${environment.endpoint}/users/${userId}/albums`);
  }

  public getPhotosByAlbum(albumsId: number): Observable<Photo[]> {
    return this.http.get<Photo[]>(`${environment.endpoint}/photos?albumId=${albumsId}`);
  }

  public createNewAlbum(album: NewAlbum): Observable<Album> {
    return this.http.post<Album>(`${environment.endpoint}/albums`, album);
  }
}
