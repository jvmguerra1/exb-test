import { TestBed } from '@angular/core/testing';

import { InterceptService } from './intercept.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('InterceptService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [HttpClient, HttpHandler]
  }));

  it('should be created', () => {
    const service: InterceptService = TestBed.get(InterceptService);
    expect(service).toBeTruthy();
  });
});
