import { TestBed, inject } from '@angular/core/testing';

import { ApiRequestsService } from './api-requests.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { MockBackend } from '@angular/http/testing';

import { Album } from '../interfaces/Album';
import { User } from '../interfaces/User';
import { NewAlbum } from '../interfaces/NewAlbum';
import { ResponseOptions } from '@angular/http';

export const getUsersMock: User[] = [
  {
    id: 1,
    name: '',
    username: '',
    email: '',
    address: {
      street: '',
      suite: '',
      city: '',
      zipcode: '',
      geo: {
        lat: '',
        lng: '',
      }
    },
    phone: '',
    website: '',
    company: {
      name: '',
      catchPhrase: '',
      bs: '',
    }
  }
];

export const getAlbumsByUserMock: Album[] = [
  {
    userId: 1,
    id: 1,
    title: '',
    numberOfPhotos: 1
  }
];

export const getPhotosByAlbumMock: Album = {
  userId: 1,
  id: 1,
  title: '',
  numberOfPhotos: 1
};

export const createNewAlbumMock: Album = {
  userId: 1,
  id: 1,
  title: '',
  numberOfPhotos: 1
};

describe('ApiRequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [HttpClient, HttpHandler, MockBackend]
  }));

  it('should be created', () => {
    const service: ApiRequestsService = TestBed.get(ApiRequestsService);
    expect(service).toBeTruthy();
  });

  it('should return correct object for successful getUsers',
    inject([ApiRequestsService, MockBackend], (service: ApiRequestsService, mockBackend: MockBackend) => {
    mockBackend.connections.subscribe((connection) => {
      connection.mockRespond(getUsersMock);
    });
    service.getUsers().subscribe(res => {
      expect(res).toEqual(getUsersMock);
    });
  }));

  it('should return correct object for successful getAlbumsByUser',
    inject([ApiRequestsService, MockBackend], (service: ApiRequestsService, mockBackend: MockBackend) => {
    mockBackend.connections.subscribe((connection) => {
      connection.mockRespond(getAlbumsByUserMock);
    });
    service.getAlbumsByUser(1).subscribe(res => {
      expect(res).toEqual(getAlbumsByUserMock);
    });
  }));

  it('should return correct object for successful getPhotosByAlbum',
    inject([ApiRequestsService, MockBackend], (service: ApiRequestsService, mockBackend: MockBackend) => {
    mockBackend.connections.subscribe((connection) => {
      connection.mockRespond(getPhotosByAlbumMock);
    });
    service.getPhotosByAlbum(1).subscribe(res => {
      expect(res).toEqual(getPhotosByAlbumMock);
    });
  }));

  it('should return correct object for successful createNewAlbum',
    inject([ApiRequestsService, MockBackend], (service: ApiRequestsService, mockBackend: MockBackend) => {
    mockBackend.connections.subscribe((connection) => {
      connection.mockRespond(createNewAlbumMock);
    });
    const newAlbum: NewAlbum = {title: '', userId: 1};
    service.createNewAlbum(newAlbum).subscribe(res => {
      expect(res).toEqual(createNewAlbumMock);
    });
  }));

  it('should return correct error object string for getUsers',
    inject([ApiRequestsService, MockBackend], (service: ApiRequestsService, mockBackend: MockBackend) => {
    mockBackend.connections.subscribe((connection) => {
      connection.mockError(new Response(JSON.stringify({ error: 'My custom error' })));
    });
    service.getUsers().subscribe(
      res => fail('Expected error'),
      err => expect(err.json().error).toBe('My custom error')
    );
  }));

  it('should return correct error object string for getAlbumsByUser',
    inject([ApiRequestsService, MockBackend], (service: ApiRequestsService, mockBackend: MockBackend) => {
    mockBackend.connections.subscribe((connection) => {
      connection.mockError(new Response(JSON.stringify({ error: 'My custom error' })));
    });
    service.getAlbumsByUser(1).subscribe(
      res => fail('Expected error'),
      err => expect(err.json().error).toBe('My custom error')
    );
  }));

  it('should return correct error object string for getPhotosByAlbum',
    inject([ApiRequestsService, MockBackend], (service: ApiRequestsService, mockBackend: MockBackend) => {
    mockBackend.connections.subscribe((connection) => {
      connection.mockError(new Response(JSON.stringify({ error: 'My custom error' })));
    });
    service.getPhotosByAlbum(1).subscribe(
      res => fail('Expected error'),
      err => expect(err.json().error).toBe('My custom error')
    );
  }));

  it('should return correct error object string for createNewAlbum',
    inject([ApiRequestsService, MockBackend], (service: ApiRequestsService, mockBackend: MockBackend) => {
    mockBackend.connections.subscribe((connection) => {
      connection.mockError(new Response(JSON.stringify({ error: 'My custom error' })));
    });
    const newAlbum: NewAlbum = {title: '', userId: 1};
    service.createNewAlbum(newAlbum).subscribe(
      res => fail('Expected error'),
      err => expect(err.json().error).toBe('My custom error')
    );
  }));
});
