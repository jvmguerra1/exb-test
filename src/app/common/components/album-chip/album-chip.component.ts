import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Album } from '../../interfaces/Album';

@Component({
  selector: 'app-album-chip',
  templateUrl: './album-chip.component.html',
  styleUrls: ['./album-chip.component.scss']
})
export class AlbumChipComponent {

  @Input() albumInfo: Album;
  @Input() closeable: boolean;
  @Output() close: EventEmitter<number> = new EventEmitter<number>();

  public emitClear() {
    this.close.emit(this.albumInfo.id);
  }
}
