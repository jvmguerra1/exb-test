import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumChipComponent } from './album-chip.component';
import { MatIconModule, MatButtonModule } from '@angular/material';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { of, Observable } from 'rxjs';

export class FakeLoader implements TranslateLoader {
  public getTranslation(lang: string): Observable<any> {
      return of({});
  }
}

describe('AlbumChipComponent', () => {
  let component: AlbumChipComponent;
  let fixture: ComponentFixture<AlbumChipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ MatIconModule, MatButtonModule,
        TranslateModule.forRoot({
          loader: { provide: TranslateLoader, useClass: FakeLoader }
        })
      ],
      declarations: [ AlbumChipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumChipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
