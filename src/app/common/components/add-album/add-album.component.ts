import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-add-album',
  templateUrl: 'add-album.component.html',
})
export class AddAlbumComponent {

  constructor(
    public dialogRef: MatDialogRef<AddAlbumComponent>,
    @Inject(MAT_DIALOG_DATA) public title: string) {}

  public onCancel(): void {
    this.dialogRef.close();
  }

}
