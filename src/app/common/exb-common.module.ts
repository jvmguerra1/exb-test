import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { InterceptService } from './services/intercept.service';
import { DragAndDropModule } from 'angular-draggable-droppable';
import { AddAlbumComponent } from './components/add-album/add-album.component';
import { AlbumChipComponent } from './components/album-chip/album-chip.component';
import { TranslateModule } from '@ngx-translate/core';
import {
  MatCardModule,
  MatButtonToggleModule,
  MatButtonModule,
  MatIconModule,
  MatFormFieldModule,
  MatDialogModule,
  MatInputModule,
  MatSnackBarModule,
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { PhotoCardComponent } from './components/photo-card/photo-card.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    TranslateModule.forChild(),
    FormsModule,
    DragAndDropModule,
    MatCardModule,
    MatButtonToggleModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatFormFieldModule,
    MatDialogModule,
    MatSnackBarModule
  ],
  exports: [
    AlbumChipComponent,
    AddAlbumComponent,
    PhotoCardComponent
  ],
  providers: [
    InterceptService,
    HttpClient,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true
    }
  ],
  declarations: [
    AddAlbumComponent,
    AlbumChipComponent,
    PhotoCardComponent
  ],
  entryComponents: [AddAlbumComponent]
})
export class ExbCommonModule { }
