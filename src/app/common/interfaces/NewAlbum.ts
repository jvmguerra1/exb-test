export interface NewAlbum {
    userId: number;
    title: string;
}
