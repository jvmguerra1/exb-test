export const common = {
    'COMMON': {
        'PHOTOS': 'Photos',
        'CANCEL': 'Cancel',
        'CONFIRM': 'Confirm',
        'ALBUM_TITLE': 'Album title'
    }
};
