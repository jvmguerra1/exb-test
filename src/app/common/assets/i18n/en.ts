import { dashboard } from './dashboard/en';
import { common } from './common/en';

export const translations = {
    ...dashboard,
    ...common
};
