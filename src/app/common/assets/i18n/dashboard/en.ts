export const dashboard = {
    'DASHBOARD': {
        'PHOTOS': {
            'TITLE': 'Photos',
            'ATOZ': 'A - Z',
            'ZTOA': 'Z - A',
            'PLACEHOLDER': 'Select an album first',
            'ERROR': 'There was an error processing the request',
            'EMPTY': 'The albums selected are empty'
        },
        'ALBUMS': {
            'TITLE': 'Albums',
            'SELECTALL': 'Select All',
            'DRAGNDROP': 'Drag and Drop to Photos section',
            'PLACEHOLDER': 'Select a user first',
            'ERROR': 'There was an error processing the request',
            'EMPTY': 'User\'s album list is empty'
        },
        'USERS': {
            'TITLE': 'Users',
            'ATOZ': 'A - Z',
            'ZTOA': 'Z - A',
            'ERROR': 'There was an error processing your request',
            'EMPTY': 'There are no users registered'
        },
        'SELECT_USER': 'Select a user first',
        'ALBUM_TITLE_MISSING': 'Please input the album\'s title',
        'ALBUM_CREATED_SUCCESS': 'Album created! ID: ',
        'ALBUM_CREATED_ERROR': 'There was an error processing your request',
    }
};
