import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { AppComponent } from './app.component';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { ExbCommonModule } from './common/exb-common.module';
import { translations } from './common/assets/i18n/en';
import { AppRoutingModule } from './app.routing.module';
import { ChildrenOutletContexts } from '@angular/router';

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    ExbCommonModule,
    TranslateModule.forRoot(),
    DashboardModule
  ],
  declarations: [AppComponent],
  providers: [ChildrenOutletContexts],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(private translateService: TranslateService) {
    this.translateService.setDefaultLang('en');
    this.translateService.use('en');
    this.translateService.setTranslation(this.translateService.currentLang, translations, true);
  }
}
